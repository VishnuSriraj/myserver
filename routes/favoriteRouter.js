const express = require('express');
const bodyParser = require('body-parser');
const favoriteRouter = express.Router();
const authenticate = require('../authenticate');
const cors = require('./cors');

const Favorite = require('../models/favorite');

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
        Favorite.find({ 'user': { "_id": req.user._id } })
            .populate('user').populate('dishes')
            .then((dishes) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dishes);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favorite.findOne({ 'user': { "_id": req.user._id } })
            .then((favorite) => {
                if (favorite != null) {
                    for (var i = 0; i < req.body.length; i++) {
                        if (favorite.dishes.indexOf(req.body[i]._id) == -1) {
                            favorite.dishes.push(req.body[i]._id);
                        } else {
                            err = new Error('Dish already added to favorites');
                            err.status = 404;
                            return next(err);
                        }
                    }
                    favorite.save()
                        .then((favorite) => {
                            console.log('Favorites Updated ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }), (err) => next(err);
                } else {
                    Favorite.create({ user: req.user._id, dishes: req.body })
                        .then((favorite) => {
                            console.log('Favorites Created ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites');
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favorite.remove({ 'user': { "_id": req.user._id } })
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

favoriteRouter.route('/:dishId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        res.statusCode = 403;
        res.end('GET operation not supported on /favorites/' + req.params.dishId);
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favorite.findOne({ 'user': { "_id": req.user._id } })
            .then((favorite) => {
                if (favorite != null) {
                    if (favorite.dishes.indexOf(req.params.dishId) == -1) {
                        favorite.dishes.push(req.params.dishId);
                    } else {
                        err = new Error('Dish already added to favorites');
                        err.status = 404;
                        return next(err);
                    }
                    favorite.save()
                        .then((favorite) => {
                            console.log('Favorites Updated ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }), (err) => next(err);
                } else {
                    Favorite.create({ user: req.user._id, dishes: req.params.dishId })
                        .then((favorite) => {
                            console.log('Favorites Created ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites/' + req.params.dishId);
    })
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        Favorite.findOne({ 'user': { "_id": req.user._id } })
            .then((favorite) => {
                index = favorite.dishes.indexOf(req.params.dishId);
                if (index >= 0) {
                    favorite.dishes.splice(index, 1);
                }
                favorite.save()
                    .then((favorite) => {
                        console.log('Favorite Deleted ', favorite);
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorite);
                    }), (err) => next(err);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

module.exports = favoriteRouter;